$(document).ready(function(){
	$(document).mouseup(function(e){
		// Hide Side Navigasi
		var menu = $('#navigasi');
		if (!menu.is(e.target) // The target of the click isn't the container.
		&& menu.has(e.target).length === 0) // Nor a child element of the container
		{
			document.getElementById("navigasi").style.width = "0px";
			document.getElementById("hide-body").classList.remove("block-body");
			document.body.classList.remove("stop-scrolling"); 
		}
	});

	// Get Today Date
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = today.getFullYear();

	today = mm + '/' + dd + '/' + yyyy;

	$('#datepicker').val(today);

	$('#datepicker').pickadate({
		selectMonths: true, // Enable Month Selection
		selectYears: 10, // Creates a dropdown of 10 years to control year
		format: 'mm/dd/yyyy',
	});
});    

// Open Side Navigasi
function navhead() {
	document.getElementById("navigasi").style.width = "60%";
	document.getElementById("hide-body").classList.add("block-body");
	document.body.classList.add("stop-scrolling");
}

// Pinch Zoom
let pinchZoomEnabled = false;
function enablePinchZoom(pdfViewer) {
	let startX = 0, startY = 0;
	let initialPinchDistance = 0;        
	let pinchScale = 1;    
	const viewer = document.getElementById("viewer");
	const container = document.getElementById("viewerContainer");
	const reset = () => { startX = startY = initialPinchDistance = 0; pinchScale = 1; };
	// Prevent native iOS page zoom
	//document.addEventListener("touchmove", (e) => { if (e.scale !== 1) { e.preventDefault(); } }, { passive: false });
	document.addEventListener("touchstart", (e) => {
		if (e.touches.length > 1) {
			startX = (e.touches[0].pageX + e.touches[1].pageX) / 2;
			startY = (e.touches[0].pageY + e.touches[1].pageY) / 2;
			initialPinchDistance = Math.hypot((e.touches[1].pageX - e.touches[0].pageX), (e.touches[1].pageY - e.touches[0].pageY));
		} else {
			initialPinchDistance = 0;
		}
	});
	document.addEventListener("touchmove", (e) => {
		if (initialPinchDistance <= 0 || e.touches.length < 2) { return; }
		if (e.scale !== 1) { e.preventDefault(); }
		const pinchDistance = Math.hypot((e.touches[1].pageX - e.touches[0].pageX), (e.touches[1].pageY - e.touches[0].pageY));
		const originX = startX + container.scrollLeft;
		const originY = startY + container.scrollTop;
		pinchScale = pinchDistance / initialPinchDistance;
		viewer.style.transform = `scale(${pinchScale})`;
		viewer.style.transformOrigin = `${originX}px ${originY}px`;
	}, { passive: false });
	document.addEventListener("touchend", (e) => {
		if (initialPinchDistance <= 0) { return; }
		viewer.style.transform = `none`;
		viewer.style.transformOrigin = `unset`;
		PDFViewerApplication.pdfViewer.currentScale *= pinchScale;
		const rect = container.getBoundingClientRect();
		const dx = startX - rect.left;
		const dy = startY - rect.top;
		container.scrollLeft += dx * (pinchScale - 1);
		container.scrollTop += dy * (pinchScale - 1);
		reset();
	});
}

document.addEventListener('webviewerloaded', () => {
	if (!pinchZoomEnabled) {
		pinchZoomEnabled = true;
		enablePinchZoom();
	}
});

// Form Efect
// Show Password
// function showPass() {
	// var pass = document.getElementById("password");
		// if (pass.type === "password") {
	// pass.type = "text";
		// $('#p-show').text("Hide");
	// } else {
		// pass.type = "password";
		// $('#p-show').text("Show");
	// }
// }

// Show Password Confirm
// function showCPass() {
	// var cpass = document.getElementById("cpassword");

	// if (cpass.type === "password") {
		// cpass.type = "text";
		// $('#cp-show').text("Hide");
	// } else {
		// cpass.type = "password";
		// $('#cp-show').text("Show");
	// }
// }
